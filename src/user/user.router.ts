import express, { Express, Request, Response } from "express";
import validateRequset from "../middleware/validateRequest";
import { createUserHandler } from "./user.controller";
import { createUserSchema } from "./user.schema";
const router = express.Router();

router.post("/", validateRequset(createUserSchema), createUserHandler);

export default router;
