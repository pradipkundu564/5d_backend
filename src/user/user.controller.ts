import { Request, Response } from "express";
import { omit } from "lodash";
import { createUser } from "./user.service";
import log from "../logger";

export async function createUserHandler(req: Request, res: Response) {
    try {
        const user = await createUser(req.body);
        return res.send(omit(user.toJSON(), "password"));
    } catch (err: any) {
        console.log('-----', err.message);
        return res.status(400).send([err.message]);
    }
}