import { omit } from "lodash";
import { DocumentDefinition, FilterQuery } from "mongoose";
import { getError } from "../utils/error-handling";
import User, { UserDocument } from "./user.model";

export async function createUser(userInput: DocumentDefinition<UserDocument>) {
    try {
        return await User.create(userInput);
    } catch (err: any) {
        throw new Error(getError(err));
    }
}



export async function findUser(query: FilterQuery<UserDocument>) {
    return User.findOne(query).lean();
}

export async function validatePassword({
    email,
    password,
}: {
    email: UserDocument["email"];
    password: string;
}) {
    const user = await User.findOne({ email });

    if (!user) {
        return false;
    }

    const isValid = await user.comparePassword(password);

    if (!isValid) {
        return false;
    }

    return omit(user.toJSON(), "password");
}