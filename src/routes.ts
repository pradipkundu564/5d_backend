import { Express, Request, Response } from "express";
import momentRouter from "./moment/moment.router";
import sessionRouter from "./session/session.router";
import userRouter from "./user/user.router";


export default function(app: Express) {
    app.get(["/", "/healthCheck"], (req: Request, res: Response) => res.sendStatus(200));
    app.use('/api/user', userRouter);
    app.use('/api/moment', momentRouter);
    app.use('/api/session', sessionRouter);
}