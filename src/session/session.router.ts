import express, { Express, Request, Response } from "express";
import validateRequset from "../middleware/validateRequest";
import { createSessionSchema } from "./session.schema";
import { createSessionHandler, getSessionsHandler, invalidateSessionHandler } from "./session.controller";
import requiresUser from "../middleware/requiresUser";
const router = express.Router();

router.post("/", validateRequset(createSessionSchema), createSessionHandler);
router.get("/", requiresUser, getSessionsHandler);
router.delete("/", requiresUser, invalidateSessionHandler);

export default router;