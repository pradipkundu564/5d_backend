import { json } from "body-parser";
import log from "../logger";

interface Error {

}

export function getError(error: any,) {
    try {
        log.error(error);
        console.log(error);
        if(error.code == '11000') {
            return error.keyValue[Object.keys(error.keyValue)[0]]+ ' already regstered';
        }
    } catch (error: any) {
        return 'unknown error';
    }
}