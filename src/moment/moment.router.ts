import express, { Express, Request, Response } from "express";
import requiresUser from "../middleware/requiresUser";
import validateRequset from "../middleware/validateRequest";
import { createMomentHandler, deleteMomentHandler, getMomentHandler, updateMomentHandler, saveImageHandler, getImage } from "./moment.controller";
import { createMomentSchema, updateMomentSchema } from "./moment.schema";
const router = express.Router();

const multer = require("multer");
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req: any, file: any, cb: any) {
        console.log('--------');
        console.log(file);
        cb(null, `./uploads/`);
    },

    filename: function (req: any, file: any, cb: any) {
        cb(null, file.fieldname + '-' + Date.now().toString() + path.extname(file.originalname));
    }
});


const upload = multer({
    storage: storage,
    // limits: { fileSize: 4 * 1024 * 1024 }
})

router.post('/', upload.any('image'), validateRequset(createMomentSchema), requiresUser,  createMomentHandler);
router.get('/', requiresUser, getMomentHandler);
router.put('/', validateRequset(updateMomentSchema), requiresUser, updateMomentHandler);
router.delete('/:momentId', requiresUser, deleteMomentHandler);
router.get('/image', requiresUser, getImage);
// router.post('/image', requiresUser, upload.any('image'),  saveImageHandler);
// router.delete('/image', requiresUser, deleteImageHandler);

export default router;
