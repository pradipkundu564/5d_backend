import { omit } from "lodash";
import { DocumentDefinition, FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import Moment, { MomentDocument } from "./moment.model";

export async function createMoment(momentInput: DocumentDefinition<MomentDocument>) {
    try {
        return await Moment.create(momentInput);
    } catch (err: any) {
        throw new Error(err);
    }
}


export async function findMoments(query: FilterQuery<MomentDocument>) {
    return Moment.find(query).lean();
}


export async function findAndUpdate(query: FilterQuery<MomentDocument>, update: UpdateQuery<MomentDocument>, options: QueryOptions) {
    return await Moment.findOneAndUpdate(query, update, options);
}

export async function findAndDelete(query: FilterQuery<MomentDocument>) {
    return await Moment.deleteOne(query);
}
