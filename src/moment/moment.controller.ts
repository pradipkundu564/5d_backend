import log from "../logger";
import { Request, Response } from "express";
import { omit, get } from "lodash";
import { createMoment, findAndUpdate, findMoments, findAndDelete } from "./moment.service";
import multer from "multer";
import path from "path";
import fs from "fs"
// import uploadFile from '../middleware/upload'

export async function createMomentHandler(req: Request, res: Response) {
    try {
        console.log(req.files, req.body)

        //@ts-ignore
        let images: string[] = req.files?.map((file: any) => {
            return file.filename;
        })
        //@ts-ignore
        req.body['user'] = get(req, "user._id");
        req.body['images'] = images;
        const moment = await createMoment(req.body);
        return res.send(omit(moment.toJSON(), "password"));
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}

export async function getMomentHandler(req: Request, res: Response) {
    try {
        //@ts-ignore
        const userId = get(req, "user._id");
        const moments = await findMoments({ user: userId });
        return res.send(moments);
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}

export async function updateMomentHandler(req: Request, res: Response) {
    try {
        //@ts-ignore
        const userId = get(req, "user._id");
        const momentId = get(req, "body.momentId")
        const moments = await findAndUpdate({ user: userId, _id: momentId }, req.body, { new: true });
        return res.send(moments);
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}

export async function deleteMomentHandler(req: Request, res: Response) {
    try {
        //@ts-ignore
        const userId = get(req, "user._id");
        const momentId = get(req, "params.momentId");
        const moments = await findAndDelete({ user: userId, _id: momentId });
        return res.send(moments);
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}

export async function saveImageHandler(req: Request, res: Response) {
    try {
        console.log('--------', req.files)
    } catch (error: any) {

    }
}

export async function getImage(req: Request, res: Response) {
    try{

        var options = {
            root: path.join(__dirname + '/uploads')
        };
    
        var fileName = req.query.image;
        // let file = fs.readFileSync(fileName);
        // res.sendFile(path.resolve(fileName), options, function (err) {
        //     if (err) {
        //         console.log(err);
        //     } else {
        //         console.log('Sent:', fileName);
        //     }
        // });
        let indexPath = path.join(__dirname, "../../uploads/"+ fileName);
        res.sendFile(indexPath);
    } catch(error) {
        console.log(error);
        res.send(error);
    }
}