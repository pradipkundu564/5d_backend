import mongoose from "mongoose";
import { UserDocument } from "../user/user.model";

export interface MomentDocument extends mongoose.Document {
  user: UserDocument["_id"];
  images: string[];
  title: string;
  tags: string[];
  createdAt: Date;
  updatedAt: Date;
}

const MomentSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    images: [{ type: String }],
    title: { type: String, required: true},
    tags: [{ type: String }]
  },
  { timestamps: true }
);

const Moment = mongoose.model<MomentDocument>("Moment", MomentSchema);

export default Moment;