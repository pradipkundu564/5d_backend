import { object, string, ref, array } from "yup";

export const createMomentSchema = object({
    body: object({
        images: array(),

        title: string()
            .max(100, "Comment is too long - should be 100 chars maximum.")
            .required("Title is required"),

        tags: array()
    }),

});

export const updateMomentSchema = object({
    body: object({
        title: string()
            .max(100, "Title is too long - should be 100 chars maximum."),

        tags: array()
    }),

});