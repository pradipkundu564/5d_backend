import express from "express";
import config from "config";
import { deserializeUser } from "./middleware/deserializeUser";
import log from "./logger"
import connet from "./db/connect"
import routes from './routes';

const port = config.get("port") as number;
const host = config.get("host") as string;

const app = express();
app.use(express.static('uploads'))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, authorization, x-refresh, responseType");
  next();
});

app.use(deserializeUser);



app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.listen(port, host, () => {
  log.info(`Server listing at http://${host}:${port}`);
  connet();
  routes(app);
});